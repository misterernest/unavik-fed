// ES 7
// includes buscan el elemnto dentro de una array
let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];
let numeroBuscado = 7;
if (numbers.includes(numeroBuscado , 8)) {
    console.log(`Se encontro el numero ${numeroBuscado}`);
} else {
    console.log(`No se encontro el numero ${numeroBuscado}`);
}
//array.inlcudes(numero buscado, indice)
console.log(numbers.includes(3 , 8));
console.log(numbers.includes(3));

// exponente
// ES5

console.log(Math.pow(5, 3));

// ES7
console.log(5 ** 3)

/////////////////////////////////////////////////////////////////
//ES8
let nomProducto = 'Balon';
let dimension = '25 pulgadas';
let material = 'cuero';

let productoES6 = {
    nomProducto,
    dimension,
    material,
}

let objEnArray = Object.entries(productoES6);
console.log(objEnArray);

// acceder al array de dos dimensiones
objEnArray[0][0] // devolver el primero atributo del objeto en array

valorObjEnArray = Object.values(productoES6);


// PADDING
let cadena = 'hello';
console.log(cadena.padStart(8, 'hi '));
console.log(cadena.padEnd(8, ' hi'));


// trailing de comas
let objNuevo = {
    name: 'Ernesto',
    age: '38',
    country: 'COL',
    profesion: 'Software developer',
    language: 'Español',
}


