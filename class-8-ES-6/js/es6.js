// CONCATENAR
let texto1 = "Hola como estas? \n"
    + "bien y tú"

let texto2 = `Hola esta esta la primera linea
esta es la segunda linea`;

console.log(texto1);
console.log(texto2);

// DESESTRUCTURACION

let name1 = "Ernesto";
let lastName1 = 'Campos';

let persona = {
    name: `${name1} ${lastName1}`,
    age: '38',
    country: 'COL'
}

console.log(persona.name, persona.age, persona.country);

let {name, age, country} = persona;
console.log(name, age, country)

// SPREAD OPERATOR
let team1 = ['Ricardo', 'Blanca', 'Guilleromo'];
let team2 = ['Karem', 'Aragón', 'Bren'];
let education = ['Ernesto', ...team1, ...team2];


// CREAR OBJETOS DESDE VARIABLES

let nomProducto = 'Balon';
let dimension = '25 pulgadas';
let material = 'cuero';

let producto = {
    nomProducto: nomProducto,
    dimension: dimension,
    material: material,
}
console.log(producto)

let productoES6 = {
    nomProducto,
    dimension,
    material,
}
console.log(productoES6)
