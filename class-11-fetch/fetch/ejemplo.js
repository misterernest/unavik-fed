const fetch = require('node-fetch'); // Solo es para nodeJS
const API = 'https://rickandmortyapi.com/api/character/';

const getData = async (api_url) => {
    try {
        const response = await fetch(api_url);
        const data = await response.json();
        return data
    } catch (e) {
        console.error(`Error en funcion getData error: ${e}`);
    }
}

/*
getData().then(
    response => { console.log(response)}
)
*/
const imprimeInfo = async () => {
    try {
        const characters = await getData(API);
        const location = await getData(characters.results[0].origin.url)
        console.log(`Numero de personajes: ${characters.info.count}`);
        console.log(`Id del personaje ${characters.results[0].id}`);
        console.log(`Nombre del personaje ${characters.results[0].name}`)
        console.log(`Nombre de la ubicación de ${characters.results[0].name} es: ${location.dimension}`)
    } catch (e) {
        console.error(e)
    }

}

imprimeInfo();
