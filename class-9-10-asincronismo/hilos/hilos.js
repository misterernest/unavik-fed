const repeticiones = () => {
    let final = 10000;
    for (let i = 1; i <= final; i++ ) {
        console.error(`Este ciclo usa el hilo por ${final} repeticiones`);
    }
}

const imprimirConsola = () => {
    console.log('Este es mi mensaje');
}

const asincronico = () => {
    console.warn(new Date);
    setTimeout(() => {
        let date = new Date;
        console.warn(date);
    }, 5000)
}