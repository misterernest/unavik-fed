const callData = require('../utils/callData');
const API = 'https://rickandmortyapi.com/api/character/';

const otraFuncion = async (url_api) => {
    try {
        const data = await callData(url_api);
        const personaje = await callData(`${url_api}${data.results[0].id}`);
        const origenPersonaje = await  callData(personaje.origin.url);
        console.log(`Numero de personajes: ${data.info.count}`);
        console.log(`Nombre del personaje: ${personaje.name}`);
        console.log(`Dimension al que pertence ${personaje.name}: ${origenPersonaje.dimension}`)
        console.log(`Nombre de la dimensión: ${origenPersonaje.name}`)
    } catch (e) {
        console.error(e)
    }
}

otraFuncion(API);