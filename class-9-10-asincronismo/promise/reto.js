const callData = require('../utils/callData');
const API = 'https://rickandmortyapi.com/api/character/';

callData(API)
    .then( data => {
        console.log(API);
        console.log(data.info.count)
        return callData(`${API}${data.results[0].id}`)
    }).then( data => {
        console.log(`Nombre del personaje: ${data.name}`)
        return callData(`${data.origin.url}`)
    }).then(data => {
        console.log(`Nombre de la dimensión: ${data.name}`)
    }).catch( error => console.error(error));

