const algoVaAPasar = (edad) => {
    return new Promise((resolve, reject) => {
        if(edad >= 18) {
            resolve('Esta persona es mayor de edad, pueda votar')
        } else {
            reject('Advertencia este personaje, debe ir a la casa')
        }
    })
}

const algoVaAPasar2 = () => {
    return new Promise((resolve, reject) => {
        if(false) {
            setTimeout(() => {
                resolve('Este es un setTimeOut');
            }, 3000)
        } else {
            const error = new Error('Algo fallo en esta solicitud');
            reject(error);
        }
    })
}
let edad = 34
/*
algoVaAPasar(edad)
    .then( response => console.log(response))
    .catch(error => console.error(error));

algoVaAPasar2()
.then( response => console.log(response))
    .then(console.log('Por aquí pasó'))
    .catch(error => console.error(error));
*/

Promise.all([algoVaAPasar(edad), algoVaAPasar2()])
    .then(response => console.log(`Respuest1 ${response[0]}
    Respuesta 2: ${response[1]}`))
    .catch(error => console.error(error));
