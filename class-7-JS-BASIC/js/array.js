let estudiantes = [
    'Bren',
    'Blanca',
    'David',
    'Guillermo',
]
// longitud del array
console.log(estudiantes.length);

// PUSH agregar un elemento al final

estudiantes.push('Ernesto');


// POP eliminar el ultimo elemento y devuelve el elemento eliminado
estudiantes.pop();

// UNSHIFT agrega un elemento al principio
estudiantes.unshift('Ernesto')

// SHIFT elimina un elemento al principio y devuelve el elemento eliminado
estudiantes.shift()

// indexOF devuelve la posición de la primera concurrencia
estudiantes.indexOf("Blanca");

let productos = [
    {nombre: 'Carrito',
        valor: '100'},
    {nombre: 'Pista de carreras',
        valor: '300'},
    {nombre: 'Avion',
        valor: '200'},
    {nombre: 'PS',
        valor: '2000'}
]
// RECORRER ARRAYS
// find recorre el array hasta encontrar la primera coincidencia de la validacion
productos.find(producto => producto.valor < 250);
//{nombre: "Carrito", valor: "100"}

// filter devuelve todos los que cumplan un condicion
productos.filter(producto => producto.valor < 250)

// MAP recorre cada elemento del array y devuelve un nuevo array modificado
let nuevoArrayMap = productos.map(producto => Number(producto.valor))

// otro ejmeplo de MAP
// map que hace descuento del 10% del producto que cueste mas de $250
let nuevoArrayMap2 = productos.map(producto => {
    if (Number(producto.valor) > 250) {
        return {
            nombre: producto.nombre,
            valor: Number(producto.valor) * 0.90
        };
    } else {
        return {
            nombre: producto.nombre,
            valor: Number(producto.valor)
        };
    }
});