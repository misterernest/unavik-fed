// HOISTING
/*var nombre
console.log(nombre)
nombre = 'Ernesto';*/
/*var nombre;
nombre = 'Ernesto';*/


/*var saludoNormal = function () {
    console.log('hola');
}

saludoNormal();*/



// COERCION
// COERCION IMPLICITA
// 4 + "7" = "47"
//
// COERCION EXPLICITA
// let a = 20;
// let b = "30";
// a + b
// "2030"
// a = Number(a);
// 20
// b = Number(b);
// 30
// a + b
// 50
// b = String(b);
// "30"
// a = String(a)
// "20"

// TRUTHY Y FALSY

// FALSE
// 0, NaN, null, undefined, false, ""
/////////////////////////////////////
//
// es verdadero siempre que sea diferente de cero
// " ", cuando el string algun caracter
// [], {} array y objetos vacios
// function

/*if (function (){}) {
    console.log('Es verdadero');
} else {
    console.log('Es falso');
}*/

let miFuncion = function (a, b) {
    let total = a + b;
    return (() => console.log(`el resultado es ${total}`))(total)
}
// CONDICIONALES
/*
* == igual
* === identico
* > >= mayor que o mayor o igual que
* <= menor igual que
* < menor que
* ! negar
* != diferente
* !== no identico
*
* binarios
* +, -, * , /, %
*
* OPERADORES LOGICOS
* NOT !
* AND &&
* OR ||
* */
/*a = true;
if (!a) {
    console.log('Es verdadero')
} else {
    console.log('Es falso')
}*/

/*let edad = 30
if (edad < 18) {
    console.log('Es menor de edad')
} else if( edad >= 18 && edad < 30) {
    console.log("Es mayor de edad, tiene mucho que aprender")
} else {
    console.log('Es mayor de edad, y tiene experiencia')
}*/
/*let posLlegada = 1;
switch (posLlegada) {
    case 1:
        console.log('Medalla de oro');
        break;

    case 2:
        console.log('Medalla de plata');
        break;

    case 3:
        console.log('Medalla de bronce');
        break;

    default:
        console.log('No tiene medalla')
        break;
}*/

