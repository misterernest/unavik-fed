// FUNCIONES

// Funciones declarativas
let primerApellido = 'Vanegas';
function saludar(nombre) {
    console.log(`Hola, ${nombre} ${primerApellido}`);
    console.log('Hola ' + nombre);
    return '';
}

// Funciones Anonimas o de expresión
let saludoAlegre = function (nombre) {
    console.log(`Feliz día, ${nombre}`)
}

// Arrow function
let saludoFormal = (nombre) => console.log(`Buenos tardes, ${nombre}`);

//console.log(nombre); //este es un error
//funciones console
/*
console.log(`Esto es mensaje dejando registro`);
console.info(`Esto es mensaje de información`);
console.warn('Este es un mensaje de advertencia');
console.error('Aquí hay un error');
*/

let saludoInformal = (
    nombre = "Guillermo",
    apellido = "Sai"
) => {
    console.log(`Hi, ${nombre} ${apellido}`);
}



