// ejemplo JS 1
/*alert('Este es el primer ejemplo JS CLASE 6');
alert('Este es el segundo ejemplo JS CLASE 6');*/

// TIPOS DE DATOS
// TIPOS PRIMITIVOS
    //STRING
    "hola"
    'hola'
    //`Hola` - es otra forma solo permitida en funciones

    //NUMBER
    45
    22.2
    0.4564

    //Boolean
    true
    false
    // NULL
    null

    // UNDEFINED
    undefined

// TIPOS OBJETO

// ARRAY
var arreglo = [1, 2, 3];

// OBJETOS
var objeto = {
    nombre: "Ernesto",
    apellido: "Campos"
};

// Como saber el tipo de objeto de typeof

a = 20;

if (typeof a == 'number') {
    console.log("Este es un numero");
}





