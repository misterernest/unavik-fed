/*
// DECLARAR var, let, cons
// EJEMPLO DE SCOPE

// EJEMPLO 2
var global1 = 1;
function miFuncion () {
    console.log(global1)
}
console.log( global1);

// SCOPE GLOBAL EJEMPLO 1
function miFuncion () {
    var global2 = 1;
    console.log(global2)
}
console.log( global2);

// SCOPE LOCAL  LET
let globalLet1 = 1;
function miFuncion () {
    console.log(globalLet1)
}
console.log( globalLet1);

// SCOPE LOCAL  LET

function miFuncion () {
    let globalLet1 = "esta es local";
    console.log(globalLet1)
}
let globalLet1 = "esta es global";
console.log( globalLet1);
* */
/*let profesion = "Admin Sys"
profesion = "Developer"*/
// CONS variables

//const ID = '11111111';

// SCOPE DE CONSTANTE
/*let globalLet2 = 'global';
function miFuncion () {
    console.log(globalLet2);
    const VAR_CONST = "scope LOCAL";
    console.log(VAR_CONST);

}*/
// console.log( globalLet2);
//console.log(VAR_CONST); // MANEJA SCOPE COM LO MANEJA LET

// FUNCTION
// DECLARATIVA
function miFuncion() {
    console.log('esta es mi funcion');
    return true;
}

// funcion anonima o de expresion
let miFuncion2 = function () {
    console.log('esta es mi funcion de expresion');
    return false;
}

// arrow function
let arrowFunction1 = () => console.log('Esta es mi arrow function');

// recibe parametros
let arrowFunction2 = (num1, num2) => num1 + num2;

// concatenar
let nombre = "Ernesto"
let apellido = "Campos"
console.log("hola, " + nombre + " " + apellido);
console.log(`hola, ${nombre} ${apellido}, y sumo 4 + 5 = ${4+5}`);




// TEMPLATE STRING

